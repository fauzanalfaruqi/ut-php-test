<?php
include './includes/database.php';

$db = new database();
$mahasiswa = $db->tampil();
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Daftar Mahasiswa</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
  </head>
  <body>
    <h1>Daftar Mahasiswa</h1>
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Umur</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($mahasiswa as $mhsw) : ?>
          <tr>
            <td><?php echo $mhsw['id']; ?></td>
            <td><?php echo $mhsw['nama']; ?></td>
            <td><?php echo $mhsw['alamat']; ?></td>
            <td><?php echo $mhsw['umur']; ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </body>
</html>