# Solusi Test PHP - UT

## 1.A Membuat database dan tabel menggunakan MySQL

Query DDL:

    CREATE DATABASE IF NOT EXISTS db_akademik;

    USE db_akademik;

    CREATE TABLE IF NOT EXISTS mahasiswa (
        id INT AUTO_INCREMENT PRIMARY KEY,
        nama VARCHAR(100),
        alamat VARCHAR(255),
        umur INT
    );

Penampakan di phpMyAdmin setelah query tersebut dijalankan.

![phpMyAdmin](./src/phpMyAdmin.png)

## 1.B Membuat 4 file script CRUD PHP

Berikut adalah struktur folder dari projek yang dibuat:

    ├── includes
    │   └── database.php
    ├── hapus.php
    ├── simpan.php
    └── tampil.php

#### 1. `./includes/database.php`: meng-handle koneksi dan berkomunikasi dengan database. 

    <?php

    class database {
        var $host = "localhost";
        var $uname = "root";
        var $pass = "";
        var $db = "db_akademik";

        function koneksi_database() {
            $conn = mysqli_connect($this->host, $this->uname, $this->pass, $this->db);
            if (!$conn) {
                die("Koneksi gagal: " . mysqli_connect_error());
            }

            return $conn;
        }

        function tampil() {
            $conn = $this->koneksi_database();

            $sql = "SELECT id, nama, alamat, umur FROM mahasiswa;";
            $result = $conn->query($sql);

            $mahasiswa = [];
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $mahasiswa[] = $row;
                }
            }

            $conn->close();
            return $mahasiswa;
        }

        function simpan($nama, $alamat, $umur) {
            $conn = $this->koneksi_database();

            $sql = "INSERT INTO mahasiswa (nama, alamat, umur) VALUES (?, ?, ?);";

            $statement = $conn->prepare($sql);
            $statement->bind_param("ssi", $nama, $alamat, $umur);

            if ($statement->execute()) {
                echo "Berhasil menambahkan mahasiswa.";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $statement->close();
            $conn->close();
        }

        function hapus($id) {
            $conn = $this->koneksi_database();

            $sql = "DELETE FROM mahasiswa WHERE id = ?";

            $stmt = $conn->prepare($sql);
            $stmt->bind_param("i", $id);
            
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }

            $stmt->close();
            $conn->close();
        }
    }

#### 2. `./tampil.php`: sebuah halaman yang menampilkan data semua mahasiswa dalam bentuk tabel.

    <?php
    include './includes/database.php';

    $db = new database();
    $mahasiswa = $db->tampil();
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <title>Daftar Mahasiswa</title>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
        </head>
        <body>
            <h1>Daftar Mahasiswa</h1>
            <table>
            <thead>
                <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Umur</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mahasiswa as $mhsw) : ?>
                <tr>
                    <td><?php echo $mhsw['id']; ?></td>
                    <td><?php echo $mhsw['nama']; ?></td>
                    <td><?php echo $mhsw['alamat']; ?></td>
                    <td><?php echo $mhsw['umur']; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </body>
    </html>

Penampakan halaman tampil.php

![phpMyAdmin](./src/tampil.png)

#### 3. `./simpan.php`: sebuah halaman yang berisi form untuk menambah data mahasiswa.

    <?php
    include './includes/database.php';

    $db = new database();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['submit'])) {
                $nama = $_POST['nama'];
                $alamat = $_POST['alamat'];
                $umur = $_POST['umur'];
                
                $db->simpan($nama, $alamat, $umur);
                header('Location: tampil.php');
            }
        }
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <title>Simpan Data Mahasiswa</title>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
        </head>
        <body>
            <h1>Simpan Data Mahasiswa</h1>
            <form method="post">
                <label for="nama">Nama:</label>
                <input type="text" name="nama" id="nama" required>
                <label for="alamat">Alamat:</label>
                <input type="text" name="alamat" id="alamat" required>
                <br>
                <label for="umur">Umur:</label>
                <input type="number" name="umur" id="umur" required>
                <br>
                <button type="submit" name="submit">Simpan</button>
            </form>
        </body>
    </html>


Penampakan halaman simpan.php

![phpMyAdmin](./src/simpan.png)

#### 3. `./hapus.php`: sebuah halaman yang berisi form untuk menghapus data mahasiswa berdasarkan number id dari inputan.

    <?php
    include './includes/database.php';

    $db = new database();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            if (!is_numeric($id)) {
                header('Location: hapus.php?error=3');
                exit();
            }

            $result = $db->hapus($id);

            if ($result) {
                header('Location: tampil.php');
            } else {
                header('Location: hapus.php?error=1');
            }
        } else {
            header('Location: hapus.php?error=2');
        }
    }

    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
            <title>Hapus Data Mahasiswa Berdasarkan ID</title>
        </head>
        <body>
            <h2>Hapus Data Mahasiswa Berdasarkan ID</h2>
            <?php
            if (isset($_GET['error'])) {
                $error_code = $_GET['error'];
                if ($error_code == 1) {
                    echo "<p>Error: Gagal menghapus mahasiswa. Data tidak ditemukan.</p>";
                } else if ($error_code == 2) {
                    echo "<p>Error: Parameter 'id' tidak ditemukan.</p>";
                } else if ($error_code == 3) {
                    echo "<p>Error: ID tidak valid.</p>";
                }
            }
            ?>
            <form action="./hapus.php" method="post">
                <label for="id">ID:</label>
                <input type="text" id="id" name="id">
                <button type="submit">Hapus</button>
            </form>
        </body>
    </html>

Penampakan halaman hapus.php

![phpMyAdmin](./src/hapus.png)

## Source

Untuk full code dari proyek ini dapat di temukan [disini](https://gitlab.com/fauzanalfaruqi/ut-php-test.git)