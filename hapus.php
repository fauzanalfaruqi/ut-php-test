<?php
include './includes/database.php';

$db = new database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];

        if (!is_numeric($id)) {
            header('Location: hapus.php?error=3');
            exit();
        }

        $result = $db->hapus($id);

        if ($result) {
            header('Location: tampil.php');
        } else {
            header('Location: hapus.php?error=1');
        }
    } else {
        header('Location: hapus.php?error=2');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
        <title>Hapus Data Mahasiswa Berdasarkan ID</title>
    </head>
    <body>
        <h2>Hapus Data Mahasiswa Berdasarkan ID</h2>
        <?php
        if (isset($_GET['error'])) {
            $error_code = $_GET['error'];
            if ($error_code == 1) {
                echo "<p>Error: Gagal menghapus mahasiswa. Data tidak ditemukan.</p>";
            } else if ($error_code == 2) {
                echo "<p>Error: Parameter 'id' tidak ditemukan.</p>";
            } else if ($error_code == 3) {
                echo "<p>Error: ID tidak valid.</p>";
            }
        }
        ?>
        <form action="./hapus.php" method="post">
            <label for="id">ID:</label>
            <input type="text" id="id" name="id">
            <button type="submit">Hapus</button>
        </form>
    </body>
</html>