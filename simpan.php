<?php
include './includes/database.php';

$db = new database();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['submit'])) {
            $nama = $_POST['nama'];
            $alamat = $_POST['alamat'];
            $umur = $_POST['umur'];
            
            $db->simpan($nama, $alamat, $umur);
            header('Location: tampil.php');
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Simpan Data Mahasiswa</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
    </head>
    <body>
        <h1>Simpan Data Mahasiswa</h1>
        <form method="post">
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama" required>
            <label for="alamat">Alamat:</label>
            <input type="text" name="alamat" id="alamat" required>
            <br>
            <label for="umur">Umur:</label>
            <input type="number" name="umur" id="umur" required>
            <br>
            <button type="submit" name="submit">Simpan</button>
        </form>
    </body>
</html>
