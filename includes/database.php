<?php

class database {
    var $host = "localhost";
    var $uname = "root";
    var $pass = "";
    var $db = "db_akademik";

    function koneksi_database() {
        $conn = mysqli_connect($this->host, $this->uname, $this->pass, $this->db);
        if (!$conn) {
            die("Koneksi gagal: " . mysqli_connect_error());
        }

        return $conn;
    }

    function tampil() {
        $conn = $this->koneksi_database();

        $sql = "SELECT id, nama, alamat, umur FROM mahasiswa;";
        $result = $conn->query($sql);

        $mahasiswa = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $mahasiswa[] = $row;
            }
        }

        $conn->close();
        return $mahasiswa;
    }

    function simpan($nama, $alamat, $umur) {
        $conn = $this->koneksi_database();

        $sql = "INSERT INTO mahasiswa (nama, alamat, umur) VALUES (?, ?, ?);";

        $statement = $conn->prepare($sql);
        $statement->bind_param("ssi", $nama, $alamat, $umur);

        if ($statement->execute()) {
            echo "Berhasil menambahkan mahasiswa.";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $statement->close();
        $conn->close();
    }

    function hapus($id) {
        $conn = $this->koneksi_database();

        $sql = "DELETE FROM mahasiswa WHERE id = ?";

        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $id);
        
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }

        $stmt->close();
        $conn->close();
    }
}